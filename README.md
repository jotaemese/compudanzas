# compudanzas

this repository contains the script and source files for the compudanzas site in gemini and the web

* generated in web/ : [https://compudanzas.net](https://compudanzas.net)
* generated in gem/ : [gemini://compudanzas.net](gemini://compudanzas.net/)

# generate site

the generator script requires lua. 

it was developed using lua 5.3.3 but it probably works with previous versions.

in order to call the script and build the site:

```sh
$ lua generasitio.lua
```

## archive

the `etc/generador/` directory contains the previous generator files and `README.md`, written in bash, awk and python.

# .gmo format

the same format as gmi (gemtext), but with the following line types:

* `+` add this line to the html file but not to the gmi file
* `&` add this line to the gmi file but not to the html file

# wikilinks

## inline wikilinks

the generator can parse and convert inline {wikilinks}, as many as needed per line

* in html, they get converted to an inline link
* in gemtext, the links are added after the line with the text

.gmo text:

```
see the {roadmap} for updates that want to happen.
```

converted to html:

```html
<p>
see the <a href='./roadmap.html'>roadmap</a> for updates that want to happen.
</p>
```

converted to gemtext:

```gemtext
see the roadmap for updates that want to happen.
=> ./roadmap.gmi roadmap
```

spaces inside the wikilinks are converted to underscores.

## gemtext-like wikilinks

the generator can also parse normal links in gemtext, one per line

* in html, it gets converted to an inline link
* in gemtext, the link is left as is

.gmo text:

```
=> ./references.gmi {references}
```

converted to html:

```html
<p><a href='./references.html'>references</a></p>
```

converted to gemtext:

```
=> ./references.gmi references
```

# license

[peer production license](https://wiki.p2pfoundation.net/Peer_Production_License)
