# gemini
lang=es
protocolo del "small internet" para documentos de hipertexto:

* minimalista en su especificación
* una conexión por documento
* la prioridad es el (hiper)texto
* comunicación cifrada
* presentación queda a cargo del programa cliente

se les llama "cápsulas" a lo que en la web llamaríamos "sitios".

# software

este sitio está servido desde un programa de "cosecha propia", chamorrx:

=> https://tildegit.org/sejo/chamorrx/ código fuente de chamorrx

además hemos creado varias herramientas basadas en gemtext utilizando {awk}.

el generador que utilizamos para manejar el sitio se llama {esglua}

# enlaces

## sobre gemini

=> https://gemini.circumlunar.space/ project gemini (web)
=> gemini://gemini.circumlunar.space/ project gemini (gemini)

## caracolito

compudanzas está hospedada en la comunidad caracolito: comunidad naciente en torno al "internet lento y pequeño", el protocolo gemini en español, e ideales pachapunk.

=> gemini://caracolito.mooo.com/ caracolito (gemini)

en caracolito hay un agregador de cápsulas con contenido en castellano:

=> gemini://caracolito.mooo.com/deriva/ bot en deriva (gemini)

## para navegar

usamos y recomendamos:

### en escritorio

=> https://gmi.skyjake.fi/lagrange/ lagrange (web)
=> gemini://skyjake.fi/lagrange/ lagrange (gemini)
=> https://github.com/makeworld-the-better-one/amfora/ amfora (web)
=> http://bombadillo.colorfield.space/ bombadillo (web)
=> https://tildegit.org/solderpunk/AV-98 av-98 (web)

### navegador web / proxy

=> https://portal.mozz.us/ gemini portal

### y más

=> //gemini.circumlunar.space/software/ gemini software

# notas

## raw request

usando openssl se puede hacer una request de la siguiente forma:

```
echo "gemini://compudanzas.net/" | openssl s_client -connect compudanzas.net:1965 -servername "compudanzas.net" -quiet -crlf -no_ssl3 -no_tls1 -no_tls1_1
```

## fingerprint

para confirmar fingerprint como cliente:

```
openssl s_client -showcerts -servername compudanzas.net -connect compudanzas.net:1965 </dev/null | openssl x509 -fingerprint
```

actualmente el resultado de la fingerprint debe ser:

```
8B:64:25:58:88:1B:6F:8D:F9:92:E2:40:A5:55:F1:96:9C:81:F3:DC
```

localmente desde el servidor, la fingerprint la obtengo de la siguiente manera:

```
openssl x509 -in cert.pem -noout -fingerprint
```
