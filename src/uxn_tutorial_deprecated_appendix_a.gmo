# uxn tutorial appendix a: the on-screen debugger

once we set the system colors in varvara, as described in {uxn tutorial day 2}, we can use the on-screen debugger to aid us in figuring out what is happening with the stacks and memory during the runtime of our programs.

# the debugger

if you tried using the F2 key while running your program before today, you would have found that apparently nothing happened.

that was because the on-screen debugger uses the screen device, and therefore needs the system colors to be set.

now that you have some system colors set, run your program and press the F2 key: you'll see several elements now!

=> ./img/screenshot_uxn-debugger.png screenshot of the on-screen debugger using the assigned system colors
* there are some lines and a crosshair in the center, drawn with color 2
* at the top left, there are four rows of eight bytes each, represented in hexadecimal and drawn with color 1; these 32 bytes show the deeper contents of the stack, with the stack "top" highlighted using color 2
* below, there is a single byte drawn with color 2: it corresponds to the address of the top of the return stack that we discuss on {uxn tutorial day 5}
* finally, there is another set of 32 bytes, drawn with color 3; these show the contents of the first section of the zero page in the main memory, discussed on {uxn tutorial day 4}

remember: you can use the F1 key to switch between different zoom levels.

take a look at the representation of the stack: if you didn't change the values i suggested above, you'll see the following numbers at the top left:

``` 2c e5 0c
[2c] e5 0c
```

what are these numbers?

2ce5 is the short we assigned to the blue components of the system colors, and 0c is the i/o address of the short corresponding to .System/b ! (can you calculate and enumerate what are the numerical addresses of each of the color components in the system device?)

those values are the leftovers from the last operation on our program, the DEO2 that set the blue component of the system colors.

we can think of the highlight in the leftmost 2c, as an arrow pointing leftwards to the "top" of the stack. it current position implies that the stack is empty, as there are no more elements to its left.

note that the stack memory is not erased when taking elements out of it, what changes is the value of the address that points to its top.

# stack debugging test

let's try appending to our program the example code we discussed above, adding it after setting the system colors:

```
#0004 #0008 ADD2
```

run it, open the debugger, and see the contents of the stack.

what does it mean what you see?

if everything went alright, you'll see:

```
00 0c [00] 08
```

if we think of the highlight as an arrow pointing left towards the top of the stack, we'll see that its position corresponds with the result that we would have expected!

```
00 0c <- top
```

000c is the result of the addition that was performed, that it is now stored in the stack!

the highlighted 00, and the 08 to its right, correspond to the 0008 of our second operand. they were used by the ADD2 instruction already, but they are left unused in the stack memory. they would stay there until overwritten.

in general, if our program is functioning alright, we will see the highlight of the top of the stack always at the top left position.

otherwise, it means that our operations with the stack were left unbalanced: there were more elements added to it than element removed from it.

# support

if you found this tutorial to be helpful, consider sharing it and giving it your {support} :)
