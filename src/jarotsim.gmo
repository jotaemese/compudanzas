# jarotsim

jarotsim is a playground for the discovery, exploration and livecoding of turing machines. 

its objective is to promote experimentation and visualization of these foundational computational machines in a playful and joyful way. 

=> ./img/screenshot_jarotsim_banner.png four rows of tiles, each one with a different pattern of tiles and with a bird with a hat in a different pose and position

# demo

+ <iframe title="jarotsim - demo" src="https://spectra.video/videos/embed/a6854535-4275-477c-8d7f-39607e0c6b10" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>
=> https://spectra.video/w/myCQ4NUGgLAcC4N1ErDq83?start=0s jarotsim - demo video

the music in the video is: xxrnbplanet - Dominic Loretti
=> https://linktr.ee/dominicloretti Dominic Loretti

# download

+ <iframe src="https://itch.io/embed/1476533" width="552" height="167" frameborder="0"><a href="https://compudanzas.itch.io/jarotsim">jarotsim by compudanzas</a></iframe>
=> https://compudanzas.itch.io/jarotsim download jarotsim

jarotsim is programmed using LÖVE 11.4 and it is distributed as a .love file.

you can play this file in your mobile or not-so-mobile device(s) by downloading LÖVE for linux, macOS, windows or android:

=> https://love2d.org/ LÖVE

# the basics

=> ./img/screenshot_jarotsim_detail.png a bird with a hat standing on a row of tiles with different designs

the bird with a hat stands...

* over a long, possibly infinite, row of tiles,
* in a pose, one out of four possible poses,
* over one tile in the row, which has one out of four possible designs.

the bird with a hat has a table of rules that tells what to do next.

the table of rules is read as follows: given a specific pose of the bird with a hat, and a specific tile design in the row where the bird is standing:

* which new design should the used for the given tile?
* which should be the new pose of the bird with a hat?
* in which direction should the bird move, in order to stand in a different tile?

=> ./img/screenshot_jarotsim-rule-active.png an active rule as described in the section below

the process is repeated over and over again. it might never end, unless the table of rules tells the bird with a hat to stop.

and yes, in case you didn't know yet, all of that is what a turing machine does!

# modes

jarotsim has two modes:

* edit mode: where you can set up and modify the aspects of the procedure to follow.
* timeline mode: where you can see several iterations of the process.

## edit mode

=> ./img/screenshot_jarotsim_edit.png screenshot of the edit mode as described below

### top row: current state

the top section of this screen allows you to modify:

* the state of the row of tiles: click on a tile to rotate between the four possible designs
* the current pose of the bird with a hat: click on the bird to rotate between the four possible poses
* the position of the bird on the row of tiles: click on the arrows to shift the row right or left

you can clear the row of tiles by clicking on the bin icon at the top right corner.

### table of rules

the rest of the screen allows you to modify the table of rules.

there is one rule for each combination of pose and tile; therefore there are sixteen rules.

depending on the size of the screen, you might be able to see them all, or you might need to use the arrows to rotate between the pages of the table.

a rule consists of the following elements:

=> ./img/screenshot_jarotsim-rule-active.png an active rule as described below

* at the top, a combination of pose and tile design: these are fixed.
* the tile design that will be used to replace the current tile: you can click on it to rotate between the four possible designs.
* the pose of the bird in the next step: you can click on the bird to rotate between the four possible poses, and a fifth one that indicates that the bird will halt.
* where in the row the bird should move, right or left of the current tile. you can click on the empty space at the left or right of the tile to have the bird move there.

the table of rules highlights the rule that would currently apply, given the pose of the bird and the tile design where they are standing.

the halt pose will hide the new tile design, and additionally it will never be highlighted.

=> ./img/screenshot_jarotsim-rule-halt.png a halt rule as described above

## timeline mode

=> ./img/screenshot_jarotsim_timeline.png screenshot of the timeline mode as described below

the timeline mode accumulates several past iterations of the process so that you can see its evolution.

when the screen has been filled, the timeline automatically scrolls so that the current state is at the bottom.

the previous states are always arranged relative to the bird with a hat in the current state.

it's very encouraged to use this mode as a source of visuals for algoraves.

# toolbar

the toolbar at the bottom has the following buttons, from left to right:

* step: apply the current rule and get to the next step.
* play/pause: have the bird with a hat move automatically according to the rules.
* speed: rotate between three different speed levels for the automatic movement.
* mode: toggle between edit and timeline mode.

the toolbar can be used regardless of the mode that jarotsim is on. 

therefore you can e.g. see how the rules are applied live, and even modify them while the bird with a hat is moving.

note that the bird will pause when it arrives at a rule with a halt pose. you won't be able to step or press play until you change the rule, the current pose, and/or the current tile design.

# explore

=> ./img/screenshot_jarotsim_banner.png four rows of tiles, each one with a different pattern of tiles and with a bird with a hat in a different pose and position

the initial state of jarotsim is based on a turing machine described by Wolfram in A New Kind of Science, page 80:

=> https://www.wolframscience.com/nks/p80--turing-machines/ Turing Machines: A New Kind of Science | Online by Stephen Wolfram

it will generate an ever-growing and complex sequence starting from an empty row of tiles.

feel free to modify the rules to observe the changes in the behavior of the process.

additionally, we invite you to attempt the following: 

have the bird with a hat...

* "bouncing" back and forth between two tiles.
* repeatedly setting a pattern of tiles in the row.
* setting a pattern of tiles in the row, and then stopping. what's the longest pattern that you can set before stopping? (this is basically the "busy beaver game")
* processing an "input" given as the initial state of the row of tiles. maybe the bird can duplicate it? count the amount of specific tiles? perform arithmetic?
* generating interesting visuals for your next algorave.
* and whatever else you can think of!

setting up the sixteen possible rules in the table can be a little bit overwhelming, so remember that you can "turn off" a rule, a column and/or a row by using the halting pose.

have fun!

=> ./img/icono_jarotsim_64p.png jarotsim icon: a bird with a hat

# support

we will really appreciate it if you are able to pay for jarotsim, because you will help supporting our projects of human-scale computing and movement! 

thank you! <3

# about

jarotsim is inspired by {turingsim} as a simulator of turing-machine-based performances like {d-turing} or {mub}.

the objective is to have a graphical user interface where one can edit / livecode the rules of the machine and its tape, and explore their effects.

for the moment the idea is to have a maximum of 4 states and 4 symbols. according to Wolfram, they should be more than enough to explore complex behaviors.

=> https://www.wolframscience.com/nks/p78--turing-machines/ Turing Machines: A New Kind of Science | Online by Stephen Wolfram

as of now, all of the machines described in {máquinas de turing} can be simulated in jarotsim.
