# choreutics

notes from laban's book: choreutics, rudolf laban. annotated and edited by lisa ullmann (2011)

> systematic survey of the grammatical elements of space-movement

laban's lifework consisted in developing frameworks for analyzing, notating and creating (human) movement.

# directions

there are 26 directions in the kinesphere that radiate from its center (a 27th point of direction).

they establish three planes at different levels.

the directions come from combining these 3 levels with the 9 possible point directions in each level.

## levels

* deep
* medium
* high

## point directions

* left
* right
* forward
* backward
* left forward
* left backward
* right forward
* right backward
* center

# spatial crosses

## three-dimensional cross

formed by the six principal directions:

* high center
* deep center
* medium forward
* medium backward
* medium right
* medium left

## four-diagonal cross

formed by the eight diagonal directions:

* high left forward
* high right forward
* high left backward
* high right backward
* deep left forward
* deep right forward
* deep left backward
* deep right backward

## six-diametral cross

formed by the twelve deflected directions (?)

# dynamosphere / effort

basic training:

* light movements into upward directions
* strong movements into downward directions
* straight movements into sideways (crossed) directions
* roundabout movements into sideways (open) directions
* quick movements into backward directions
* slow movements into forward directions

combining these we get the "eight fundamental dynamic actions", that correspond in the dynamosphere to the eight diagonal directions of the kinesphere (see four-diagonal cross above).

they can be indicated by using the directional signs of the kinesphere and adding the letter "S".

## the eight fundamental dynamic actions

aka the eight efforts:

when using a right limb:

* pressing/pushing: slow, strong, straight (forward deep left)
* wringing/pulling: slow, strong, roundabout (forward deep right)
* gliding: slow, light, straight (forward high left)
* floating: slow, light, roundabout (forward high right)
* punching: quick, strong, straight (backward deep left)
* slashing: quick, strong, roundabout (backward deep right)
* dabbing: quick, light, straight (backward high left)
* flicking: quick, light, roundabout (backward, high, right)

### octal encoding

possible encoding in {octal}:

+ <table>
+ <tr><th>name</th><th>dynamics</th><th>binary</th><th>oct</th></tr>
+ <tr><td>pressing</td><td>slow, strong, straight</td><td>000</td><td>0</td></tr>
+ <tr><td>wringing</td><td>slow, strong, roundabout</td><td>001</td><td>1</td></tr>
+ <tr><td>gliding</td><td>slow, light, straight</td><td>010</td><td>2</td></tr>
+ <tr><td>floating</td><td>slow, light, roundabout</td><td>011</td><td>3</td></tr>
+ <tr><td>punching</td><td>quick, strong, straight</td><td>100</td><td>4</td></tr>
+ <tr><td>slashing</td><td>quick, strong, roundabout</td><td>101</td><td>5</td></tr>
+ <tr><td>dabbing</td><td>quick, light, straight</td><td>110</td><td>6</td></tr>
+ <tr><td>flicking</td><td>quick, light, roundabout</td><td>111</td><td>7</td></tr>
+ </table>

& * pressing/pushing: slow, strong, straight (binary: 000, octal: 0)
& * wringing/pulling: slow, strong, roundabout (binary: 001, octal 1)
& * gliding: slow, light, straight (binary: 010, octal 2)
& * floating: slow, light, roundabout (binary: 011, octal 3)
& * punching: quick, strong, straight (binary: 100, octal 4)
& * slashing: quick, strong, roundabout (binary: 101, octal 5)
& * dabbing: quick, light, straight (binary: 110, octal 6)
& * flicking: quick, light, roundabout (binary: 111, octal 7)


# TODO

* add tables
* add labanotation symbols
