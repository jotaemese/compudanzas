# uxn tutorial
lang=en es->{tutorial de uxn}
welcome to this beginner's, slow-paced and comprehensive guide for programming the varvara computer based on the {uxn} core.

you can get an offline version of this guide as the {introduction to uxn programming book}!

if you prefer video, you can watch a short {intro to uxn programming} workshop that we taught as an introduction.

# day 1

in this first section of the tutorial we talk about the basics of the uxn computer called varvara, its programming paradigm in a language called uxntal, its architecture, and why you would want to learn to program it.

we also jump right in into our first simple programs to demonstrate fundamental concepts that we will develop further in the following days.

=> ./uxn_tutorial_day_1.gmi {uxn tutorial day 1}

# day 2

in this section we start exploring the visual aspects of the varvara computer: we talk about the fundamentals of the screen device so that we can start drawing on it!

we also discuss working with shorts (2-bytes) besides single bytes in uxntal.

=> ./uxn_tutorial_day_2.gmi {uxn tutorial day 2}

=> ./img/screenshot_uxn-tiles.png screenshot of the output of the program, showing 16 squares colored with different combinations of outline and fill.

# day 3

here we introduce the use of the controller device in the varvara computer: this allows us to add interactivity to our programs, and to start implementing control flow in uxntal.

we also talk about logic and stack manipulation instructions in uxntal.

=> ./uxn_tutorial_day_3.gmi {uxn tutorial day 3}

=> ./img/screenshot_uxn-draw-with-keyboard.png screenshot of a possible result of running the following program; it shows a trail drawn with filled or outlined squares.

# day 4

here we discuss the animation loop of the varvara computer, via its screen device vector!

we also talk about using the program memory as a space for data via "variables", in order to have some persistency of data during the runtime of our programs, and/or in order to save us from complex stack wrangling :)

=> ./uxn_tutorial_day_4.gmi {uxn tutorial day 4}

# day 5

here we introduce the varvara mouse device to explore more possible interactions, and we cover the remaining elements of uxntal and uxn: the return stack, the return mode and the keep mode.

we also discuss possible structures to create loops and more complex programs using these resources!

=> ./uxn_tutorial_day_5.gmi {uxn tutorial day 5}

=> ./img/screenshot_uxn-draw-with-mouse.png screenshot showing a drawing made with the mouse: wiggly lines composed of overlapped squares of two different colors

# day 6

here we talk about how we can integrate everything that we have covered in order to create even more complex subroutines and programs for the varvara computer.

we base our discussion in a recreation of the classic pong game!

besides using previous strategies and snippets of code, we cover strategies for drawing and controlling multi-tile sprites, and for checking collisions.

=> ./uxn_tutorial_day_6.gmi {uxn tutorial day 6}

=> ./img/screenshot_uxn-pong-paddles-and-ball.png screenshot showing the stage of the pong game: a couple of paddles at the sides, and a ball at the center

# day 7

here we talk about the devices in the varvara computer that we haven't covered yet: audio, file, and datetime.

this should be a light and calm end of our journey, as it has to do less with programming logic and more with the input and output conventions in these devices.

=> ./uxn_tutorial_day_7.gmi {uxn tutorial day 7}

# appendices

## appendix a: generalized rectangular tiles drawing

in this appendix we generalize the background drawing procedure discussed on day 6, into a draw-tiles subroutine that draws an arbitrary rectangle filled with a given tile.

we detail how to get to two versions of this subroutine, one that relies on heavy stack wrangling, and other one that uses variables. this in order to compare both approaches and give us a broader view of the possibilities within uxntal.

=> ./uxn_tutorial_appendix_a.gmi {uxn tutorial appendix a}

## DEPRECATED appendix a: the on-screen debugger

in this appendix we introduce the use of the on-screen debugger available in uxnemu once we set up the system colors.

=> ./uxn_tutorial_deprecated_appendix_a.gmi {uxn tutorial deprecated appendix a}

# external resources

=> https://metasyn.github.io/learn-uxn/ learn-uxn by metasyn
=> https://wiki.xxiivv.com/site/uxn.html uxn technical documentation
=> https://nchrs.xyz/site/uxn_notes.html uxn illustrated notes
=> https://wiki.xxiivv.com/site/uxntal_reference.html the uxntal opcode manual
=> https://wiki.xxiivv.com/site/uxntal_cheatsheet.html uxntal cheatsheet
=> https://git.sr.ht/~rabbits/uxn uxn repository
=> https://llllllll.co/t/uxn-virtual-computer/ llllllll forum
=> https://github.com/hundredrabbits/awesome-uxn awesome uxn: awesome things from the community

irc channel: #uxn on irc.esper.net

# instructions

this is a summary of the uxn instructions covered in each day of the tutorial.

short mode is covered on day 2, and return and keep mode are covered on day 5.

you can find a more detailed reference of all the opcodes in the uxntal opcode manual
=> https://wiki.xxiivv.com/site/uxntal_reference.html the uxntal opcode manual

## day 1

* ADD: take the top two elements from the stack, add them, and push down the result ( a b -- a+b )
* SUB: take the top two elements from the stack, subtract them, and push down the result ( a b -- a-b )
* LIT: push the next byte in memory down onto the stack
* DEO: output the given value into the given device address, both taken from the stack ( value address -- )

## day 2

* DEI: read a value into the stack, from the device address given in the stack ( address -- value )
* INC: increment the value at the top of the stack ( a -- a+1 )
* BRK: break the flow of the program, in order to close subroutines
* MUL: take the top two elements from the stack, multiply them, and push down the result ( a b -- a*b )
* DIV: take the top two elements from the stack, divide them, and push down the result ( a b -- a/b )
* SFT: take a shift value and a number to shift with that value, and shift it. the low nibble of the shift value indicates the shift to the right, and the high nibble the shift to the left ( number shift -- shiftednumber )

## day 3

* EQU: push 01 down into the stack if the top two elements of the stack are equal, or push 00 otherwise ( a b  -- a==b )
* NEQ: push 01 down into the stack if the top two elements of the stack are not equal, or push 00 otherwise ( a b  -- a!=b )
* GTH: push 01 down into the stack if the first element is greater than the second, or push 00 otherwise ( a b  -- a>b )
* LTH: push 01 down into the stack if the first element is less than the second, or push 00 otherwise ( a b  -- a<b )

* AND: perform a bitwise AND with the top two elements of the stack, and push down the result ( a b -- a&b )
* ORA: perform a bitwise OR with the top two elements of the stack, and push down the result ( a b -- a|b )
* EOR: perform a bitwise exclusive-OR with the top two elements of the stack, and push down the result ( a b -- a^b )

* JMP: unconditionally jump to the address in the stack ( addr -- )
* JCN: conditional jump: take an address and a value from the stack, and if the value is not 00, jump to the address; otherwise continue with the next instruction ( value addr -- )

* POP: Remove top element from the stack ( a -- )
* DUP: Duplicate; push a copy of the top element ( a -- a a )
* SWP: Swap; change the order of the top two elements of the stack ( a b -- b a )
* NIP: Remove the top second element of the stack ( a b -- b )
* OVR: Over; push a copy of the second top element ( a b -- a b a )
* ROT: Rotate; reorder the top three elements of the stack so that the third one is now at the top ( a b c -- b c a )

## day 4

* LDA: load and push down into the stack the value at the given absolute address ( address -- value )
* STA: store into the given absolute address the given value ( value address -- )
* LDZ: load and push down into the stack the value at the given zero page address ( address -- value )
* STZ: store into the given zero page address the given value ( value address -- )
* LDR: load and push down into the stack the value at the given relative address ( address -- value )
* STR: store into the given relative address the given value ( value address -- )

## day 5

* JSR: unconditionally jump to the address in the working stack, pushing down into the return stack the address of the next instruction in memory
* STH: take a value from the working stack and push it down into the return stack. in return mode, do the opposite.

# support

if you enjoyed this tutorial and found it helpful, consider sharing it and giving it your {support} :)
