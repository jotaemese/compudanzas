# la sala

experimental, slow, online-but-offline-first community, summoned by compudanzas.

=> https://sala.compudanzas.net la sala

# about

la sala is an attempt to gather people who are interested in human-powered computation, somatic dance, poetry, and/or the outdoors, and who might be influenced by ideas of {permacomputing}.

it arises from the desire of experimenting with sharing and connecting with others, via digital text, without having to be always on the internet.

on one hand, we wonder what could it mean to write-while-offline: the invitation is to turn off the corresponding switches, and if possible, to be outside.

on the other hand, we are interested in thoughtful and lighthearted posts. what can it mean to move beyond the immediacy of other spaces?

due to its experimental nature, many more things are waiting to be figured out.

if you are interested in joining, make sure to {contact} us. however, please note that for the moment we are only accepting people that we know already.

## a little bit of tech-talk

in la sala we prefer to avoid these topics (or tech-talk in general), but here we mention them because they are relevant to how it works.

la sala is an always-on and always-connected computer that helps us to share offline-first text-based messages thanks to the ssb protocol.

=> https://scuttlebutt.nz/ Scuttlebutt social network

to use ssb means that we can write and read posts, offline, from our devices running an app like Manyverse, and have them syncing whenever some of our contacts and us are online at the same time.

=> https://www.manyver.se/ Manyverse - a social network off the grid

la sala is an ssb room, which means that it facilitates the sharing of contents between its members when we get online.

=> https://www.youtube.com/watch?v=W5p0y_MWwDE Introducing SSB Rooms 2.0 (yt)

# acerca

la sala es un intento de reunir a personas interesadas en computación desarrollada a mano, danza somática, poesía, o los ambientes exteriores, que además puedan estar influenciadas por las ideas de la permacomputación.

la sala surge del deseo de experimentar en el compartir con otres, via texto digital, sin tener que estar siempre conectades.

por un lado, nos preguntamos qué puede implicar escribir offline: la invitación es apagar los sistemas correspondientes a telecomunicaciones, y de ser posible, escribir al aire libre.

por otro lado, nos interesan publicaciones basadas en reflexión y buen humor. ¿qué podría implicar moverse más allá de la inmediatez que nos otorgan otros espacios?

debido a la naturaleza experimental de la sala, muchas más cosas están esperando descifrarse.

si te interesa unirte, no dudes en ponerte en {contacto}. como sea, nota que por el momento solo estamos aceptando a personas que ya conocemos de otras formas.

## unas cuantas cuestiones ténicas

en la sala preferimos evitar enfocarnos mucho en este tipo de cuestiones técnicas en general, pero aquí las mencionamos porque son relevantes a su funcionamiento.

la sala es una computadora que está siempre encendida y siempre conectada y que ayuda a que nos compartamos mensajes basados en texto, "offline-first", gracias al protocolo ssb.

=> https://scuttlebutt.nz/ Scuttlebutt social network

al usar ssb lo que sucede es que podemos escribir y leer publicaciones, fuera de línea, desde nuestros dispositivos corriendo alguna app como Manyverse; estas publicaciones se sincronizan cuando algunes de nuestres contactes, y nosotres, estemos conectadas al mismo tiempo.

=> https://www.manyver.se/ Manyverse - a social network off the grid

la sala es un "ssb room", lo que significa que se encarga de facilitar este intercambio de mensajes entre les miembres de la comunidad cuando estemos en línea.
