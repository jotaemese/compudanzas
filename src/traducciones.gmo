# traducciones
lang=es
notas sobre cómo colaborar con traducciones al {wiki} de compudanzas.

para cualquier duda o comentario, no dudes ponerte en {contacto}!

# contribuciones

agradecemos las contribuciones por parte de las siguientes personas y colectivos:

=> //tralfanum.archipielago.uno/ Traducciones Alfanumérico
=> https://virgulilla.com Roboe
=> //caracolito.mooo.com/~maleza/ ~maleza

# archivos fuente

los archivos fuente a traducir se encuentran en el directorio src/ de nuestros repositorios git.

=> https://codeberg.org/sejo/compudanzas compudanzas en codeberg
=> https://tildegit.org/sejo/compudanzas compudanzas en tildegit

## formato .gmo

los archivos están en el formato ".gmo" que es básicamente gemtext (".gmi") con un par de diferencias:

### tipos de línea

existen dos nuevos tipos de línea, "+" y "&": el primero es para agregar html directamente a la salida en html, y el segundo es para agregar gemtext directamente a la salida en gemtext.

```
 + <p>html crudo para la versión web</p>
 & gemtext crudo para la versión gemini
```

por el momento, el único uso que se le da a estos tipos de línea es la creación de tablas en html, y su respectiva "traducción" a gemtext.

como ejemplo se puede consultar el archivo fuente de la página de {running}.

=> https://codeberg.org/sejo/compudanzas/src/branch/main/src/running.gmo running.gmo

existen algunas de estas tablas en el {uxn tutorial}.

como sea, funciona suficientemente bien tener los archivos en gemtext puro, sin estos tipos de líneas extra.

### wikilinks

una adición al formato que tiene que ver con el generador del wiki, es el uso de "wikilinks".

un archivo fuente puede tener un nombre de página entre llaves, y el generador lo convertirá automáticamente a un enlace correspondiente.

por ejemplo:

```
see the {roadmap} for updates that want to happen.
```

en html se convierte a un "inline link":

```
<p>
see the <a href='./roadmap.html'>roadmap</a> for updates that want to happen.
</p>
```

y en gemtext se convierte a un enlace colocado justo después de la línea en cuestión:

```
see the roadmap for updates that want to happen.
=> ./roadmap.gmi roadmap
```

notas importantes:

* por el momento, solo se permite un wikilink por línea de texto
* los espacios en un nombre se convierten a guiones bajos.
* es necesario que el archivo destino exista; de no ser así es preferible desactivar el wikilink reemplazando las llaves por <[, ]> por ejemplo:

```
encuentra en la <[ruta de trabajo]> las actualizaciones que quieren suceder.
```

la ventaja de usar esta combinación de símbolos es que después pueden ser encontrados fácilmente.

### wikilinks tipo gemtext

también es posible agregar wikilinks como enlaces normales de gemtext, siguiendo este formato:

```
=> ./traducciones.gmi {traducciones}
```

nota que la extensión de los archivos ha de ser .gmi (y no .gmo); el generador se encargará de cambiar la extensión a html en la salida web.

### idioma

la segunda línea de los archivos puede estar vacía o con un formato como el siguiente para indicar el idioma en cuestión:

```
lang=es en->{translation}
```

la primera parte indica el idioma de la página, y la segunda (opcional) indica el nombre de la página que contiene la traducción o versión en ese idioma.

## construyendo el sitio

el repositorio solo contiene el directorio src/, sin sus salidas correspondientes. para obtenerlas hay que usar el generador.

### clonando el repositorio

primero vale la pena clonar el repositorio, e.g.:

```
$ git clone https://codeberg.org/sejo/compudanzas

# ingresa al repositorio
$ cd compudanzas/
```

(hacer esto no es necesario si sigues los pasos del flujo de trabajo con git descritos abajo :)

### generando el sitio

ya en el repositorio puedes ejecutar el generador, que requiere de la existencia de python3 (y {awk}, aunque ese ya es parte de unix) en el sistema:

```
$ ./generasitio.sh
```

verás cómo ahora hay un directorio gem/ y un directorio web/, ambos con sus salidas correspondientes.

prueba crear algún archivo .gmo con un nombre único, corre el generador, y corrobora su existencia en los directorios de salida.

además, si es un archivo no referido desde ningún otro archivo, en los mensajes de salida del generador verás un mensaje como:

```
{ruta de salida}: orphan
```

que implica que es un nodo al que no le llegan conexiones.

en general la idea es que no haya nodos "huérfanos", a menos que se trate de páginas que están en estado de borrador.

# flujo de trabajo de "pull requests" con git

las siguientes notas están basadas en esta documentación de codeberg:

=> https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/ Pull requests and Git flow | Codeberg documentation

## preparación

usaremos el repositorio de codeberg pues es más fácil crear una cuenta ahí:

=> https://codeberg.org/sejo/compudanzas compudanzas en codeberg

como sea, nota que las instrucciones son idénticas si utilizas una cuenta en tildegit, solo hay que reemplazar "codeberg.org" por "tildegit.org" en las instrucciones donde aparezca.

=> https://tildegit.org/sejo/compudanzas compudanzas en tildegit

primero que nada, crea tu cuenta :)

### fork

con la sesión iniciada, ve al repositorio de compudanzas, y presiona el botón de "Fork" arriba a la derecha.

esto va a crear una copia del repositorio en tu cuenta:

```
https://codeberg.org/TU_USERNAME/compudanzas
```

### llaves ssh

configura las llaves SSH de tu cuenta en codeberg de acuerdo a estas instrucciones:

=> https://docs.codeberg.org/security/ssh-key/ Adding an SSH key to your account

### clon local

ahora puedes clonar tu repositorio a un directorio local propio.

este directorio ha de ser diferente del que hayas clonado del repositorio original (ese clon ya lo puedes eliminar y solo trabajar con este nuevo que es el de tu cuenta :)

```
$ git clone git@codeberg.org:TU_USERNAME/compudanzas.git
```

(si le pusiste contraseña a tu llave SSH, el comando te la va a solicitar antes de descargar)

### entra al directorio

entramos al directorio, y ya casi todo estará listo para colaborar!

```
$ cd compudanzas/
```

### agrega el "upstream"

por último, esto que sigue es necesario para que puedas obtener las actualizaciones que sucedan en el repositorio original (llamado "upstream" en estos contextos); más adelante lo comentamos pero por el momento solo hay que configurarlo:

```
$ git remote add upstream git@codeberg.org:sejo/compudanzas.git
```

## trabajo local

### nueva rama

IMPORTANTE: antes de hacer cualquier modificación, hay que hacer una "rama" (branch) nueva, que en estos contextos se dice que es una "feature branch" (una rama para implementar una nueva "feature", en este caso una traducción)

```
$ git checkout -b traducción_de_equis_página
```

### cambios

ahora puedes proceder a agregar y/o editar archivos .gmo dentro del directorio src/

para agregarlos al control de cambios de git, usa:

```
$ git add src/NOMBRE_DEL_ARCHIVO.gmo
$ git commit -m "DESCRIPCIÓN DEL CAMBIO QUE REALIZASTE"
```

también puedes usar lo siguiente para agregar todos los archivos que hayan tenido cambios:

```
$ git add .
$ git commit -m "DESCRIPCIÓN DEL CAMBIO QUE REALIZASTE"
```

### sincronización

cuando desees guardar tus cambios en tu repositorio remoto, usa el siguiente comando:

```
$ git push
```

(si tu llave SSH tiene contraseña, el comando te la va a solicitar)

### actualización

para mantener tu repositorio actualizado respecto a "upstream", por ejemplo justo antes de empezar a traducir un archivo, utiliza las siguientes instrucciones:

```
$ git pull --rebase upstream main
$ git pull
```

de cualquier forma, siempre conviene estar en comunicación con nosotres para que no suceda que estamos modificando considerablemente algún archivo en común.

## pull request

### comprobación previa

antes de hacer un "pull request" para incorporar tus cambios en el sitio de compudanzas, asegúrate de haber seguido los pasos anteriores, y de haber ejecutado el generador para ver si todo resultó correctamente en términos de los archivos de salida:

```
$ ./generasitio.sh
```

recuerda no hacer "wikilinks" a páginas que no existen: usa la <[notación provisional]> o crea el archivo correspondiente, con un título y al menos una breve descripción.

### crea PR

en el sitio web de codeberg, entra a la dirección correspondiente a tu fork:

```
https://codeberg.org/TU_USERNAME/compudanzas
```

nota que la interfaz, debajo del título y la descripción del repositorio, tiene un botón que dice "Rama:" (o Branch), posiblemente seguido por el nombre "main", que es el nombre de nuestra rama principal.

presiona el botón y elige la rama que creaste para tu colaboración.

a continuación presiona el botón al lado que dice "Nuevo pull request"; esto te llevará a un formulario donde se te da la opción de agregar comentarios y de crear el pull request.

¡y listo! el PR ahora se mostrará en el repositorio principal (upstream) de compudanzas, y recibiremos una notificación para revisarlo e incorporarlo.

=> https://codeberg.org/sejo/compudanzas/pulls Pull Requests - compudanzas

por nuestra parte, ya que lo revisemos e incorporemos a la rama "main", generaremos el sitio localmente, lo publicaremos en los sitios web y gemini, y te lo haremos saber: ¡muchas gracias!
