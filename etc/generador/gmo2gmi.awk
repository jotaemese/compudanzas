function nombre2Link( t, r ){ # convierte un nombre con espacios, a uno con r (e.g. "_"
	gsub(" ",r,t);
	return t
}

/^\+ /{ # literal html
	next # salta la línea
}

/^& /{ # literal gemtext
	sub(/^& /,"",$0)
	print $0
	next

}

/^=>/{ # link
	gsub(/[{}]/,"")
}

# si tienen sintaxis de wikilink, y no son líneas de enlace, agrega el link
# (el % es por las líneas de uxn jaja)
/^[^=%]*{.+}/{
	enlaces = ""
	t = $0
	while(match(t,/\{[^{}]+\}/)){ # has {key}
		nombre = substr(t, RSTART+1, RLENGTH-2)
		link = nombre2Link( nombre, "_" )
		enlace = "=> ./" link ".gmi " nombre "\n"
		enlaces = enlaces enlace
		t = substr(t, RSTART+RLENGTH)
	}

	gsub(/[{}]/,"")
	print $0
	gsub(/\n$/,"",enlaces)
	print enlaces
	next
}

# imprime líneas normamlmente

{ 
	print $0
}

# agrega footer 

END{
	print ""
	print ""
	sub("tmp/","",FILENAME)
	if(FILENAME != "home.gmo"){
		print "updated on 1" fecha 
		print "=> https://tildegit.org/sejo/compudanzas/src/branch/main/src/" FILENAME " source file"
	}
	print ""
	print "=> ./home.gmi compudanzas"
	print "=> ./contact.gmi contact"
	print "=> https://wiki.p2pfoundation.net/Peer_Production_License text, images, and code are shared with the peer production license" 
}
