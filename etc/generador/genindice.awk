# ls src/*gmo | awk -f genindice.awk

function link2nombre( t ){ # convierte un nombre con "_" a uno con espacios
	gsub("_"," ",t);
	sub(".gmo", "", t)
	return t
}

BEGIN{
	print "# index of pages"
	print 
	print "last modified, above:"
	print
}

{
	sub("src/","",$0)
	nombre = link2nombre($0) # convierte _ a espacios
	sub(".gmo",".gmi",$0)

	if(nombre!="index") # no imprimas el índice
		print "=> ./"$0" {"nombre"}"
}
